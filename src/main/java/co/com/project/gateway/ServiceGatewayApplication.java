package co.com.project.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication(
	scanBasePackages = {
		"co.com.project.gateway",
	}
)
public class ServiceGatewayApplication {

	public static void main(String... args) {
		SpringApplication.run(ServiceGatewayApplication.class);
	}

}
