package co.com.project.gateway.domain.exception;

import co.com.project.common.domain.exception.ApplicationException;
import lombok.experimental.StandardException;

@StandardException
public class ForbiddenException extends ApplicationException {
    public ForbiddenException() {
        super("Failed process in server");
    }
}
