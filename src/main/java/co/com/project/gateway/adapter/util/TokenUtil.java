package co.com.project.gateway.adapter.util;

import io.jsonwebtoken.Jwts;
import org.springframework.stereotype.Component;

@Component
public class TokenUtil {
    public static final String SECRET = "asidodfu8923u4jiuhvbc98bu9q3y8yv";

    public void validateToken(final String token) {
        Jwts.parserBuilder().setSigningKey(SECRET.getBytes()).build().parseClaimsJws(token);
    }
}

