package co.com.project.gateway.adapter.util;

import co.com.project.common.adapter.inbound.web.dto.LoggerDTO;
import co.com.project.common.adapter.inbound.web.dto.Response;
import co.com.project.common.adapter.outbound.kafka.producer.LoggerKafkaProducer;
import co.com.project.gateway.domain.exception.ForbiddenException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.experimental.UtilityClass;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpResponse;
import reactor.core.publisher.Mono;

@UtilityClass
public class HandleError {
    public Mono<Void> failed(ServerHttpResponse response) {
        try {
            response.setStatusCode(HttpStatus.FORBIDDEN);
            response.getHeaders().setContentType(MediaType.APPLICATION_JSON);
            byte[] errorBytes = new ObjectMapper().writeValueAsBytes(Response
                    .<Void>init()
                    .data(null).message("Forbidden in server").status(403).build());
            return response.writeWith(Mono.just(response.bufferFactory().wrap(errorBytes)));
        } catch (JsonProcessingException e) {
            throw new ForbiddenException();
        }

    }
}
