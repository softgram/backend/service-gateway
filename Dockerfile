FROM openjdk:20 as builder

WORKDIR /app

COPY ./build/libs/service-gateway-1.0-SNAPSHOT.jar .

ENTRYPOINT java -jar ./service-gateway-1.0-SNAPSHOT.jar